var gulp    = require('gulp');
var $       = require('gulp-load-plugins')();
var panini  = require('panini');

var sassPaths = [
    'node_modules/foundation-sites/scss',
    'node_modules/motion-ui/src',
    'node_modules/font-awesome/scss',
];

var paths = {
    javascript: [
        'node_modules/is_js/is.js',
        'node_modules/what-input/what-input.js',
        'node_modules/mobile-detect/mobile-detect.js',
        'node_modules/isotope-layout/dist/isotope.pkgd.js',
        'js/jquery.smartbanner.js',
        'js/main.js'
    ],
    images: [
        'images/**/*',
        'images/**/**/*',
    ],
    sass: [
        'node_modules/foundation-sites/scss',
        'node_modules/motion-ui/src',
        'node_modules/font-awesome/scss',
    ]
};


var UNCSS_OPTIONS = {
    html: ["src/**/*.html"]/*,
    ignore: [!!js/regexp .foundation-mq, !!js/regexp ^\.is-.*]*/
}

// Copy page templates into finished HTML files
gulp.task('pages', function() {
  gulp.src('pages/**/*.{php,html,hbs,handlebars}')
    .pipe(panini({
      root: 'pages/',
      layouts: 'layouts/',
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('pages:reset', function(cb) {
  panini.refresh();
  gulp.run('pages');
  cb();
});

gulp.task('sass', function() {
    return gulp.src('scss/all.scss')
    .pipe($.sass({
        includePaths: paths.sass,
        outputStyle: 'compressed' // if css compressed **file size**
    })
    .on('error', $.sass.logError))
    .pipe($.autoprefixer({
        browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe($.util.env.type === 'prod' ? $.uncss(UNCSS_OPTIONS) : $.util.noop())
    .pipe($.util.env.type === 'prod' ? $.sourcemaps.write() : $.util.noop())
    .pipe(gulp.dest('dist/css'));
});

gulp.task('imagemin', function(){
	return gulp.src(paths.images)
		.pipe($.imagemin({optimizationLevel: 5}))
		.pipe(gulp.dest('dist/img'))
});

gulp.task('js', function() {
    return gulp.src(paths.javascript)
    .pipe($.sourcemaps.init())
    .pipe($.concat('all.js'))
    //only uglify if gulp is ran with '--type=prod'
    .pipe($.util.env.type === 'prod' ? $.uglify() : $.util.noop())
    .pipe($.util.env.type === 'prod' ? $.sourcemaps.write() : $.util.noop())
    .pipe(gulp.dest('dist/js'));
});

gulp.task('copy', function () {
    return gulp.src(['vendor/**/*'])
    .pipe(gulp.dest('dist/vendor'));
});

gulp.task('default', ['copy', 'pages', 'sass', 'imagemin', 'js'], function() {
    gulp.watch(['{layouts,partials}/**/*.html'], ['pages:reset']);
    gulp.watch(['pages/**/*.html', 'pages/**/*.php'], ['pages']);
    gulp.watch(['scss/**/*.scss'], ['sass']);
    gulp.watch(['js/**/*.js'], ['js']);
    gulp.watch(paths.images, ['imagemin']);
});
