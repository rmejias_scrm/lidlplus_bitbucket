<!doctype html>
<html class="no-js" lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Lidl Plus - Home</title>

        <link rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico">

        <!-- IOS
        app-id: (Required.) Your app's unique identifier. To find your app ID from the iTunes Link Maker,
        type the name of your app in the Search field, and select the appropriate country and media type.
        In the results, find your app and select iPhone App Link in the column on the right.
        Your app ID is the nine-digit number in between id and ?mt.
        i.e: https://itunes.apple.com/es/app/lidl/id398474140?mt=8
        -->
        <!--meta name="apple-itunes-app" content="app-id=465984761"-->

        <!-- ANDROID
        app-id: (Required) Your app's unique identifier. To find you app ID from the Google Play link,
        type the name of your app in the Search field, and select the appropriate country and media type.
        In the results, find your app and select App Link.
        i.e : https://play.google.com/store/apps/details?id=de.sec.mobile&hl=es
        -->
    	<!--meta name="google-play-app" content="app-id=de.sec.mobile"-->
    	<!-- Android APP Icon url (optional)-->
    	<!--meta name="msapplication-TileImage" content="https://lh5.ggpht.com/JdjFkmc8NgO0JDWkegjFpIVXHq4oveTsMm9FN9de2Dt0ZYuB1_jaTT4CxskrRvY9i7kh=w300-rw" /-->


        <link rel="stylesheet" href="/css/jquery.smartbanner.css">
        <link rel="stylesheet" href="/css/all.css">
    	<link href="/css/smart-app-banner.min.css" rel="stylesheet">


        <!-- Countly -->
        <script type='text/javascript'>

            //some default pre init
            var Countly = Countly || {};
            Countly.q = Countly.q || [];

            //provide your app key that you retrieved from Countly dashboard
            Countly.app_key = "b00503e8909987e8b58f75832e53ec70b8d87db0";

            //provide your server IP or name. Use try.count.ly for EE trial server.
            //if you use your own server, make sure you have https enabled if you use
            //https below.
            Countly.url = "https://countly.lidlplus.es";

            //start pushing function calls to queue
            //track sessions automatically
            Countly.q.push(['track_sessions']);

            //track pages automatically
            Countly.q.push(['track_pageview']);

            //track clicks on last reported view automatically
            Countly.q.push(['track_clicks']);

            //track click to specific links
            Countly.q.push(['track_links']);

            //load countly script asynchronously
            (function() {
              var cly = document.createElement('script'); cly.type = 'text/javascript';
              cly.async = true;
              //enter url of script here
              cly.src = 'https://countly.lidlplus.es/sdk/web/countly.min.js';
              cly.onload = function(){Countly.init()};
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(cly, s);
            })();
        </script>

    </head>

    <body class="cupones">

        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5BCZ5D"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5BCZ5D');</script>
        <!-- End Google Tag Manager -->

        <div class="cookie-banner cookie-layer callout" data-closable>
            <p>Este portal utiliza cookies para mejorar la navegación de los usuarios a través de nuestro portal y no almacenamos ningún dato de tipo personal. Si continúas navegando, consideramos que aceptas su uso. Puedes cambiar la configuración u obtener más información sobre las cookies de LIDL <a href="politica-cookies.html">aquí</a>.</p>
            <button class="close-button close" aria-label="Dismiss alert" type="button" data-close>
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="off-canvas-wrapper">
            <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
                <div class="off-canvas position-right reveal-for-large" id="menu" data-off-canvas data-position="right">

                    <div class="off-canvas-menu hide-for-large">
                        <!-- Close button -->
                        <button class="close-button" aria-label="Close menu" type="button" data-close>
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <!-- Menu -->
                        <ul class="vertical menu">
                            <li><a href="/index.html">Inicio</a></li>
                            <li><a href="/cupones.php">Cupones</a></li>
                            <li><a href="/precios.php">Precios Lidl Plus</a></li>
                        </ul>
                    </div>

                    <div class="show-for-large">
                        <ul class="menu">
                            <!--li class="menu-text" role="menuitem"><img data-interchange="[/img/logo_lidlplus.png, small], [/img/logo_lidlplus@2x.png, retina]" alt="Lidl Plus" width="43" alt="Lidl Plus"></li-->
                            <li role="menuitem"><a   href="/">Inicio</a></li>
                            <li role="menuitem"><a  class="active"  href="/cupones.php">Cupones</a></li>
                            <li role="menuitem"><a  href="/precios.php">Precios Lidl Plus</a></li>
                        </ul>
                    </div>

                </div>
                <div class="off-canvas-content" data-off-canvas-content>
                    <!-- Page content -->

                    
                    <?php
                    /*
                    error_reporting(E_ALL);
                    ini_set('display_errors', 1);
                    ini_set('display_startup_errors',1);
                    */
                    
                    date_default_timezone_set('Europe/Madrid');
                    setlocale(LC_ALL, 'es_ES');
                    setlocale(LC_TIME, 'Spain');
                    
                    require $_SERVER["DOCUMENT_ROOT"] . '/vendor/autoload.php';
                    use Carbon\Carbon;
                    
                    Carbon::setLocale('es');
                    
                    $dias = array("domingo","lunes","martes","miércoles","jueves","viernes","sábado");
                    $meses = array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");
                    
                    
                    $id = isset($_GET['id']) ? $_GET['id'] : null;
                    
                    $webservicesURL = "https://api.lidlplus.es/v2";
                    
                    //Autenticacion
                    $user = "+11111111111";
                    $pass = "1SRypjAKUZtQmrs";
                    
                    $response = \Httpful\Request::post('https://lidlplus-prod-authentication.azurewebsites.net/token')
                        ->body('grant_type=password&username=%2B11111111111&password=1SRypjAKUZtQmrs&scope=application-api')
                        ->addHeaders(array(
                            'Content-Type' => 'application/x-www-form-urlencoded',
                            'Authorization' => 'basic ZDU0NjgxMWItMDZmNy00OWE1LThiMzItNGI2NmU3OTUyNGNhOmRkOGMyMmYyLTVlNTUtNGM5ZS05MDdhLWQyZGE4YzYxMzAzMw==',
                        ))
                        ->send();
                    
                    $token = $response->body->access_token;
                    
                    //Obtenemos los cupones de la semana
                    $response = \Httpful\Request::get($webservicesURL.'/coupons')
                        ->addHeaders(array(
                            'Authorization' => 'Bearer '. $token,
                            'Accept' => 'application/json'
                        ))
                        ->send();
                    
                    //var_dump($response->request);
                    
                    $coupons = $response->body;
                    $isCoupon = false;
                    $coupon = null;
                    
                    if(isset($coupons) && $coupons != ""){
                    
                        $dateIni = Carbon::parse($coupons[0]->StartValidityDate);
                        $dateFin = Carbon::parse($coupons[0]->EndValidityDate);
                    
                    
                        if(isset($id)){
                            foreach($coupons as $couponItem){
                                if($couponItem->Id == $id){
                                    $isCoupon = true;
                                    $coupon = $couponItem;
                                }
                            }
                        }
                    }
                    
                    ?>
                    
                    
                        <header class="header expanded row">
                            <h1>Lidl Plus</h1>
                    
                            <div class="menu-button hide-for-large" data-open="menu"><button class="menu-icon" type="button"><span class="show-for-sr">Open menu</span></button></div>
                        </header>
                    
                    <?php
                    
                        if(!$isCoupon && isset($coupons) && $coupons != ""){
                    
                            echo '
                        <section class="cupones-title expanded row">
                                <div class="left">
                                    <img data-interchange="[img/ico-cupones.png, small], [img/ico-cupones@2x.png, retina]" alt="" width="61">
                                </div>
                                <div class="right">
                                    <h2 class="subtitle">Cupones de la semana</h2>
                                    <h3>Del '.$dateIni->day.' de '.$meses[$dateIni->month-1].' al '.$dateFin->day.' de '.$meses[$dateFin->month-1].'</h3>
                                </div>
                        </section>
                    
                        <section class="cupones-list row">
                            <div class="small-10 small-offset-1 columns">
                                <ul class="row small-up-1 medium-up-2 large-up-3">';
                    
                                        foreach($coupons as $coupon){
                                            $withDescription = ($coupon->OfferDescription != '' ? 'with-offerdescription' : '');
                                            if(!$coupon->Testcoupon){
                    
                                            echo '
                                    <li class="column" data-id="'.$coupon->Name.'" data-promo="'.$coupon->PromoId.'" data-ean="'.$coupon->EANCode.'" data-type="'.$coupon->Type.'" data-family="'.$coupon->FamilyOfInterest.'">
                                        <a href="cupones.php?id='.$coupon->Id.'"><span></span></a>
                                        <div class="crop small-12 columns"><img src="'.$coupon->Image.'" alt="'.$coupon->Description.'"></div>
                                        <div class="coupon-text small-12 columns '.$withDescription.'">
                                            <div class="name small-8 columns">'.$coupon->Product.'</div>
                                            <div class="discount small-4 columns">
                                                <span class="discount-number">'.$coupon->Discount.'</span>
                                                <span class="discount-type">'.$coupon->OfferDescription.'</span>
                                            </div>
                                        </div>
                                    </li>';
                                            }
                                        }
                            echo '
                                </ul>
                            </div>
                        </section>';
                    
                    
                        }else if($isCoupon){
                            $withDescription = ($coupon->OfferDescription != '' ? 'with-offerdescription' : '');
                    
                            echo '
                        <section class="coupon-image-blured row" style="background-image:url('.$coupon->Image.');"></section>
                    
                        <section class="coupon-detail row">
                            <div class="coupon-detail-wrapper small-10 small-offset-1 large-8 large-offset-2 columns">
                                <div class="crop small-12 columns"><img src="'.$coupon->Image.'" alt="'.$coupon->Description.'"></div>
                                <div class="coupon-text small-12 columns '.$withDescription.'">
                                    <div class="description small-8 medium-9 columns">
                                        <span class="name">'.$coupon->Product.'</span>
                                        <span class="text">'.$coupon->Description.'</span>
                                    </div>
                                    <div class="discount small-4 medium-3 columns">
                                        <span class="discount-number">'.$coupon->Discount.'</span>
                                        <span class="discount-type">'.$coupon->OfferDescription.'</span>
                                    </div>
                                </div>
                                <div class="coupon-legal small-12 columns">
                                    <p>
                                        <strong>'.$coupon->FooterTitle.'</strong><br>
                                        '.$coupon->FooterDescription.'
                                    </p>
                                </div>
                            </div>
                    
                        </section>
                    
                        <section class="cupones-list from-coupon-detail row">
                            <div class="cupones-list-wrapper small-10 small-offset-1 large-8 large-offset-2 columns">
                                <h3>Más cupones</h3>
                                <ul class="row small-up-1 medium-up-2">';
                    
                                        foreach($coupons as $coupon){
                                            if($coupon->Id != $id){
                                                $withDescription = ($coupon->OfferDescription != '' ? 'with-offerdescription' : '');
                                                if(!$coupon->Testcoupon){
                    
                                                echo '
                                    <li class="column" data-id="'.$coupon->Name.'" data-promo="'.$coupon->PromoId.'" data-ean="'.$coupon->EANCode.'" data-type="'.$coupon->Type.'" data-family="'.$coupon->FamilyOfInterest.'">
                                        <a href="cupones.php?id='.$coupon->Id.'"><span></span></a>
                                        <div class="crop small-12 columns"><img src="'.$coupon->Image.'" alt="'.$coupon->Description.'>"></div>
                                        <div class="coupon-text small-12 columns '.$withDescription.'">
                                            <div class="name small-8 columns">'.$coupon->Product.'</div>
                                            <div class="discount small-4 columns">
                                                <span class="discount-number">'.$coupon->Discount.'</span>
                                                <span class="discount-type">'.$coupon->OfferDescription.'</span>
                                            </div>
                                        </div>
                                    </li>';
                                                }
                    
                                            }
                                        }
                            echo '
                                </ul>
                            </div>
                        </section>';
                    
                        }else{
                    
                            echo '
                    
                            <section class="row text-center no-disponible">
                                <div class="image small-12 columns">
                                    <img data-interchange="[img/cupones_nodisponibles.png, small], [img/cupones_nodisponibles@2x.png, retina]" alt="" width="265">
                                </div>
                                <h2 class="title small-12 columns">Próximamente</h2>
                                <p>Lo sentimos, estamos reponiendo nuestras estanterías virtuales.</p>
                                <div class="content small-12 columns">
                                    <p><a href="/">Volver a la página principal</a></p>
                                </div>
                    
                            </section>';
                    
                    
                        }
                    ?>
                    

                    <footer>
                        <div class="row">
                            <div class="column large-3">
                                <div class="lidl-plus medium-6 large-12 column">
                                    <img data-interchange="[/img/logo_lidlplus.png, small], [/img/logo_lidlplus@2x.png, retina]" alt="Lidl Plus" width="66">
                                    <p>Lidl Plus es el nuevo programa de ventajas y descuentos de Lidl.</p>
                                </div>

                                <div class="lidl medium-6 large-12 column">
                                    <img data-interchange="[/img/logo_lidl.jpg, small], [/img/logo_lidl@2x.jpg, retina]" alt="Lidl" width="62">
                                    <p><strong>Lidl - un grupo internacional.</strong><br>Somos una exitosa cadena de supermercados que apuesta por la expansión más allá de las fronteras de Europa.</p>
                                </div>
                            </div>
                            <div class="column large-8 medium-offset-1">
                                <div class="row small-up-1 medium-up-3">
                                    <div class="column">
                                        <h4>Enlaces</h4>
                                        <ul class="vertical menu secondary">
                                            <li><a href="/index.html">Inicio</a></li>
                                            <li><a href="/cupones.php">Cupones</a></li>
                                            <li><a href="/precios.php">Precios Lidl Plus</a></li>
                                            <li><a href="/faqs.html">FAQs</a></li>
                                        </ul>
                                    </div>
                                    <div class="column">
                                        <h4>Información</h4>
                                        <ul class="vertical menu secondary">
                                            <li><a href="/condiciones-uso.html">Condiciones de uso</a></li>
                                            <li><a href="/proteccion-datos.html">Protección de datos</a></li>
                                            <li><a href="/aviso-legal.html">Aviso legal</a></li>
                                            <li><a href="/empresas">Grupo empresarial Lidl</a></li>
                                        </ul>
                                    </div>
                                    <div class="column">
                                        <h4>Contacto</h4>
                                        <ul class="vertical menu secondary">
                                            <li><a href="/contacto">Formulario de contacto</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>

                </div>

            </div>
        </div>

        <script src="/js/jquery.min.js"></script>
        <script src="/js/foundation.min.js"></script>
        <script src="/js/smart-app-banner.min.js"></script>
        <script src="/js/all.js"></script>
    </body>
</html>
