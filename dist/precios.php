<!doctype html>
<html class="no-js" lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Lidl Plus - Home</title>

        <link rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico">

        <!-- IOS
        app-id: (Required.) Your app's unique identifier. To find your app ID from the iTunes Link Maker,
        type the name of your app in the Search field, and select the appropriate country and media type.
        In the results, find your app and select iPhone App Link in the column on the right.
        Your app ID is the nine-digit number in between id and ?mt.
        i.e: https://itunes.apple.com/es/app/lidl/id398474140?mt=8
        -->
        <!--meta name="apple-itunes-app" content="app-id=465984761"-->

        <!-- ANDROID
        app-id: (Required) Your app's unique identifier. To find you app ID from the Google Play link,
        type the name of your app in the Search field, and select the appropriate country and media type.
        In the results, find your app and select App Link.
        i.e : https://play.google.com/store/apps/details?id=de.sec.mobile&hl=es
        -->
    	<!--meta name="google-play-app" content="app-id=de.sec.mobile"-->
    	<!-- Android APP Icon url (optional)-->
    	<!--meta name="msapplication-TileImage" content="https://lh5.ggpht.com/JdjFkmc8NgO0JDWkegjFpIVXHq4oveTsMm9FN9de2Dt0ZYuB1_jaTT4CxskrRvY9i7kh=w300-rw" /-->


        <link rel="stylesheet" href="/css/jquery.smartbanner.css">
        <link rel="stylesheet" href="/css/all.css">
    	<link href="/css/smart-app-banner.min.css" rel="stylesheet">


        <!-- Countly -->
        <script type='text/javascript'>

            //some default pre init
            var Countly = Countly || {};
            Countly.q = Countly.q || [];

            //provide your app key that you retrieved from Countly dashboard
            Countly.app_key = "b00503e8909987e8b58f75832e53ec70b8d87db0";

            //provide your server IP or name. Use try.count.ly for EE trial server.
            //if you use your own server, make sure you have https enabled if you use
            //https below.
            Countly.url = "https://countly.lidlplus.es";

            //start pushing function calls to queue
            //track sessions automatically
            Countly.q.push(['track_sessions']);

            //track pages automatically
            Countly.q.push(['track_pageview']);

            //track clicks on last reported view automatically
            Countly.q.push(['track_clicks']);

            //track click to specific links
            Countly.q.push(['track_links']);

            //load countly script asynchronously
            (function() {
              var cly = document.createElement('script'); cly.type = 'text/javascript';
              cly.async = true;
              //enter url of script here
              cly.src = 'https://countly.lidlplus.es/sdk/web/countly.min.js';
              cly.onload = function(){Countly.init()};
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(cly, s);
            })();
        </script>

    </head>

    <body class="precios">

        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5BCZ5D"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5BCZ5D');</script>
        <!-- End Google Tag Manager -->

        <div class="cookie-banner cookie-layer callout" data-closable>
            <p>Este portal utiliza cookies para mejorar la navegación de los usuarios a través de nuestro portal y no almacenamos ningún dato de tipo personal. Si continúas navegando, consideramos que aceptas su uso. Puedes cambiar la configuración u obtener más información sobre las cookies de LIDL <a href="politica-cookies.html">aquí</a>.</p>
            <button class="close-button close" aria-label="Dismiss alert" type="button" data-close>
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="off-canvas-wrapper">
            <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
                <div class="off-canvas position-right reveal-for-large" id="menu" data-off-canvas data-position="right">

                    <div class="off-canvas-menu hide-for-large">
                        <!-- Close button -->
                        <button class="close-button" aria-label="Close menu" type="button" data-close>
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <!-- Menu -->
                        <ul class="vertical menu">
                            <li><a href="/index.html">Inicio</a></li>
                            <li><a href="/cupones.php">Cupones</a></li>
                            <li><a href="/precios.php">Precios Lidl Plus</a></li>
                        </ul>
                    </div>

                    <div class="show-for-large">
                        <ul class="menu">
                            <!--li class="menu-text" role="menuitem"><img data-interchange="[/img/logo_lidlplus.png, small], [/img/logo_lidlplus@2x.png, retina]" alt="Lidl Plus" width="43" alt="Lidl Plus"></li-->
                            <li role="menuitem"><a   href="/">Inicio</a></li>
                            <li role="menuitem"><a  href="/cupones.php">Cupones</a></li>
                            <li role="menuitem"><a  class="active"  href="/precios.php">Precios Lidl Plus</a></li>
                        </ul>
                    </div>

                </div>
                <div class="off-canvas-content" data-off-canvas-content>
                    <!-- Page content -->

                    
                    <?php
                    /*
                    error_reporting(E_ALL);
                    ini_set('display_errors', 1);
                    ini_set('display_startup_errors',1);
                    */
                    
                    date_default_timezone_set('Europe/Madrid');
                    setlocale(LC_ALL, 'es_ES');
                    setlocale(LC_TIME, 'Spain');
                    
                    require $_SERVER["DOCUMENT_ROOT"] . '/vendor/autoload.php';
                    use Carbon\Carbon;
                    
                    Carbon::setLocale('es');
                    
                    $dias = array("domingo","lunes","martes","miércoles","jueves","viernes","sábado");
                    $meses = array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");
                    
                    
                    $id = isset($_GET['id']) ? $_GET['id'] : null;
                    
                    $webservicesURL = "https://api.lidlplus.es/v2";
                    //$webservicesURL = "https://lidlplus-staging-api.azurewebsites.net/v3";
                    
                    //Obtenemos los productos con Precio Lidl Plus
                    $response = \Httpful\Request::get($webservicesURL.'/lidlplusprices')->send();
                    $productos = $response->body;
                    
                    $isProducto = false;
                    $producto = null;
                    $productHeader = null;
                    
                    if(isset($productos) && $productos != "" && count($productos) > 0){
                        //Obtenemos fechas en base al primer productos
                        $response = \Httpful\Request::get($webservicesURL.'/lidlplusprices/'.$productos[0]->Id)->send();
                        $productoFecha = $response->body;
                        $dateIni = Carbon::parse($productoFecha->StartValidityDate);
                        $dateFin = Carbon::parse($productoFecha->EndValidityDate);
                    
                    
                        if(isset($id)){
                            $response = \Httpful\Request::get($webservicesURL.'/lidlplusprices/'.$id)->send();
                            $producto = $response->body;
                    
                            if(isset($producto) && $producto != "") $isProducto = true;
                        }
                    }
                    
                    ?>
                    
                    
                        <header class="header expanded row">
                            <h1>Lidl Plus</h1>
                    
                            <div class="menu-button hide-for-large" data-open="menu"><button class="menu-icon" type="button"><span class="show-for-sr">Open menu</span></button></div>
                        </header>
                    
                    <?php
                    
                        if(!$isProducto && isset($productos) && $productos != "" && count($productos) > 0){
                    
                            echo '
                    
                        <section class="precios-title expanded row">
                            <div class="left">
                                <img data-interchange="[img/ico-precios.png, small], [img/ico-precios@2x.png, retina]" alt="" width="60">
                            </div>
                            <div class="right">
                                <h2 class="subtitle">Precios Lidl Plus</h2>
                                <h3>Del '.$dateIni->day.' de '.$meses[$dateIni->month-1].' al '.$dateFin->day.' de '.$meses[$dateFin->month-1].'</h3>
                            </div>
                        </section>
                    
                        <section class="precios-list row">
                            <div class="small-12 columns">
                                <ul class="row small-up-2 medium-up-3 large-up-4">
                                    <div class="grid-sizer small-6 medium-4 large-3 columns"></div>';
                    
                                        foreach($productos as $producto){
                    
                                            echo '
                                    <li class="product-item column '.($producto->IsVeiled ? 'veiled' : '').'" data-id="'.$producto->Id.'">
                                        <a href="precios.php?id='.$producto->Id.'"><span></span></a>
                                        <div class="product-overlay"></div>
                                        <div class="product-overlay-text">'.$producto->VeiledText.'</div>
                                        <div class="product-image small-12 columns">
                                            <div class="arrow"></div>
                                            <div class="discount-number">'.$producto->DiscountPercentage.'</div>
                                            <img src="'.$producto->ImageThumbnail.'" alt="'.$producto->Name.'">
                                        </div>
                                        <div class="product-text small-12 columns">
                                            <div class="name small-12 columns">'.$producto->ProductTitle.'</div>
                                            <div class="discount small-12 columns">
                                                <div class="price small-6 columns">'.$producto->Price.' '.$producto->PriceType.'</div>
                                                <div class="price-lidlplus small-6 columns">'.$producto->DiscountPrice.' <span>'.$producto->PriceType.'</span></div>
                                            </div>
                                        </div>
                                    </li>';
                    
                                        }
                                echo '
                                </ul>
                            </div>
                        </section>';
                    
                        }else if($isProducto){
                    
                            echo '
                    
                        <section class="precios-image-blured row" data-interchange="[img/lidlplusprices/'.preg_replace('/\s+/','',$producto->FamilyOfInterest).'.jpg, small], [img/lidlplusprices/'.preg_replace('/\s+/','',$producto->FamilyOfInterest).'@2x.jpg, retina]"></section>
                    
                        <section class="precios-detail '.($producto->IsVeiled ? 'veiled' : '').' row">
                            <div class="precios-detail-wrapper small-10 small-offset-1 columns">
                                <div class="product-image small-12 large-4 columns">
                                    <div class="percentage" data-interchange="[img/topo.png, small], [img/topo@2x.png, retina]">
                                        '.$producto->DiscountPercentage.'
                                    </div>
                                    <div class="image small-12 columns">
                                        <div class="product-overlay"></div>
                                        <div class="product-overlay-text">'.$producto->VeiledText.'</div>
                                        <img src="'.$producto->Image.'" alt="'.$producto->Name.'">
                                    </div>
                                    <div class="discount small-12 columns">
                                        <div class="price small-6 columns">
                                            <span class="price-title">Precio original</span><br>
                                            <span class="tachado">'.$producto->Price.' '.$producto->PriceType.'</span>
                                        </div>
                                        <div class="price-lidlplus small-6 columns">'.$producto->DiscountPrice.' <span>'.$producto->PriceType.'</span></div>
                                    </div>
                                </div>
                                <div class="product-text small-12 large-8 columns">
                                    <div class="description small-12 columns">
                                        <h2 class="name">'.$producto->ProductTitle.'</h2>
                                        <p class="text">'.$producto->ProductDescription.'</p>
                                    </div>
                                    <div class="block small-12 columns">
                                        <h3 class="block-title">'.$producto->Block1Title.'</h3>
                                        <p class="block-text">'.$producto->Block1Description.'</p>
                                    </div>
                                    <div class="block small-12 columns">
                                        <h3 class="block-title">'.$producto->Block2Title.'</h3>
                                        <p class="block-text">'.$producto->Block2Description.'</p>
                                    </div>
                                </div>
                            </div>
                    
                        </section>
                    
                        <section class="precios-list row">
                            <h3 class="small-10 small-offset-1 columns end">Más productos con precios Lidl Plus</h3>
                            <div class="small-10 small-offset-1 columns end">
                                <ul class="row small-up-2 medium-up-3 large-up-4">
                                    <div class="grid-sizer small-6 medium-4 large-3 columns"></div>';
                    
                                        for($i = 0; $i <= 4 && $i < count($productos); $i++){
                                            $producto = $productos[$i];
                                            if($producto->Id != $id){
                                                echo '
                    
                                    <li class="product-item column" data-id="'.$producto->Id.'">
                                        <a href="precios.php?id='.$producto->Id.'"><span></span></a>
                                        <div class="product-image small-12 columns">
                                            <div class="arrow"></div>
                                            <div class="discount-number">'.$producto->DiscountPercentage.'</div>
                                            <img src="'.$producto->ImageThumbnail.'" alt="'.$producto->Name.'">
                                        </div>
                                        <div class="product-text small-12 columns">
                                            <div class="name small-12 columns">'.$producto->ProductTitle.'</div>
                                            <div class="discount small-12 columns">
                                                <div class="price small-6 columns">'.$producto->Price.' '.$producto->PriceType.'</div>
                                                <div class="price-lidlplus small-6 columns">'.$producto->DiscountPrice.' <span>'.$producto->PriceType.'</span></div>
                                            </div>
                                        </div>
                                    </li>';
                    
                                            }
                                        }
                                echo '
                    
                                </ul>
                            </div>
                            <p class="ver-todos small-10 small-offset-1 columns end text-right">
                                <a href="/precios.php">VER TODOS</a>
                            </p>
                        </section>';
                    
                        }else{
                            echo '
                    
                            <section class="row text-center no-disponible">
                                <div class="image small-12 columns">
                                    <img data-interchange="[img/precios_nodisponibles.png, small], [img/precios_nodisponibles@2x.png, retina]" alt="" width="223">
                                </div>
                                <h2 class="title small-12 columns">Próximamente</h2>
                                <p>Lo sentimos, estamos reponiendo nuestras estanterías virtuales.</p>
                                <div class="content small-12 columns">
                                    <p><a href="/">Volver a la página principal</a></p>
                                </div>
                    
                            </section>';
                        }
                    ?>
                    

                    <footer>
                        <div class="row">
                            <div class="column large-3">
                                <div class="lidl-plus medium-6 large-12 column">
                                    <img data-interchange="[/img/logo_lidlplus.png, small], [/img/logo_lidlplus@2x.png, retina]" alt="Lidl Plus" width="66">
                                    <p>Lidl Plus es el nuevo programa de ventajas y descuentos de Lidl.</p>
                                </div>

                                <div class="lidl medium-6 large-12 column">
                                    <img data-interchange="[/img/logo_lidl.jpg, small], [/img/logo_lidl@2x.jpg, retina]" alt="Lidl" width="62">
                                    <p><strong>Lidl - un grupo internacional.</strong><br>Somos una exitosa cadena de supermercados que apuesta por la expansión más allá de las fronteras de Europa.</p>
                                </div>
                            </div>
                            <div class="column large-8 medium-offset-1">
                                <div class="row small-up-1 medium-up-3">
                                    <div class="column">
                                        <h4>Enlaces</h4>
                                        <ul class="vertical menu secondary">
                                            <li><a href="/index.html">Inicio</a></li>
                                            <li><a href="/cupones.php">Cupones</a></li>
                                            <li><a href="/precios.php">Precios Lidl Plus</a></li>
                                            <li><a href="/faqs.html">FAQs</a></li>
                                        </ul>
                                    </div>
                                    <div class="column">
                                        <h4>Información</h4>
                                        <ul class="vertical menu secondary">
                                            <li><a href="/condiciones-uso.html">Condiciones de uso</a></li>
                                            <li><a href="/proteccion-datos.html">Protección de datos</a></li>
                                            <li><a href="/aviso-legal.html">Aviso legal</a></li>
                                            <li><a href="/empresas">Grupo empresarial Lidl</a></li>
                                        </ul>
                                    </div>
                                    <div class="column">
                                        <h4>Contacto</h4>
                                        <ul class="vertical menu secondary">
                                            <li><a href="/contacto">Formulario de contacto</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>

                </div>

            </div>
        </div>

        <script src="/js/jquery.min.js"></script>
        <script src="/js/foundation.min.js"></script>
        <script src="/js/smart-app-banner.min.js"></script>
        <script src="/js/all.js"></script>
    </body>
</html>
