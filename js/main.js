

$(function(){

    $(document).foundation();

    cookieBanner();

    if (Foundation.MediaQuery.current == "small" || Foundation.MediaQuery.current == "medium") {
        //Off Canvas Menu
        setHeight();

        $('.off-canvas').on('opened.zf.offcanvas', function() {
            $("body").css("overflow", "hidden");
        });

        $('.off-canvas').on('closed.zf.offcanvas', function() {
            $("body").css("overflow", "auto");
        });

        $('.off-canvas a[href*="#que-es"], .off-canvas a[href*="#descubre"]').on("click", function(){
            $('.off-canvas').foundation('close');
        });
    }

    //Control Scroll para modificar icono Menu
    var nav = $('header');
    var topNav = (typeof(nav[0]) != "undefined" ? nav.offset().top : 0);

    /*if(!$("body").hasClass("home")){
        $(".menu-button").addClass("with-background");
    }else{

        $(window).scroll(function () {
            if ($(window).scrollTop() > topNav+nav.height()-150) {
                $(".off-canvas .show-for-large").addClass("white");

            } else {
                $(".off-canvas .show-for-large").removeClass("white");

            }
        });
    }*/

    //Masonry Precios Lidl Plus

    /*$('.precios-list ul').isotope({
        itemSelector: '.product-item',
        percentPosition: true,
        masonry: {
            columnWidth: '.grid-sizer'
        }
    });*/

    var md = new MobileDetect(window.navigator.userAgent);

    if(md.is("iOS")){
        //Hide Android Download icon
        $(".android").hide();

        $(".descarga-link").attr("href", "https://itunes.apple.com/us/app/lidl-plus-zaragoza/id1133864850?ls=1&mt=8");

    }else if(md.is("AndroidOS")){
        //Hide iOS Download icon
        $(".ios").hide();

        $(".descarga-link").attr("href", "https://play.google.com/store/apps/details?id=com.lidl.eci.lidlplus.es");

    }else{
        $(".descarga .button.blue").hide();
        $(".descarga-link").hide();
    }

    //Smart Banner
    //if(navigator.vendor.indexOf("Apple")!=0) $.smartbanner({ daysHidden: 0, daysReminder: 0});

    //Countly track
    $(".descarga .button.blue").on("click", function(){
        var store = "";
        if(md.is("iOS")) store = "App Store";
        else if(md.is("AndroidOS")) store = "Play Store";

        Countly.q.push(['add_event', {
            key:"App Download",
            segmentation: {
                "Store": store
            }
        }]);
    });

    $(".descarga .google-download").on("click", function(){
        Countly.q.push(['add_event', {
            key:"App Download",
            segmentation: {
                "Store": "Play Store"
            }
        }]);
    });

    $(".descarga .ios-download").on("click", function(){
        Countly.q.push(['add_event', {
            key:"App Download",
            segmentation: {
                "Store": "App Store"
            }
        }]);
    });

    $(".cupones-list a").on("click", function(){
        var cupon = $(this).parent();

        Countly.q.push(['add_event', {
            key:"Coupon",
            segmentation: {
                "Id": cupon.attr("data-id"),
                "PromoID": cupon.attr("data-promo"),
                "EAN": cupon.attr("data-ean"),
                "Type": cupon.attr("data-type"),
                "FamilyInteres": cupon.attr("data-family")
            }
        }]);
    });

    $(".product-item a").on("click", function(){
        var product = $(this).parent();

        Countly.q.push(['add_event', {
            key:"Product Lidl Plus",
            segmentation: {
                "Id": product.attr("data-id")
            }
        }]);
    });

});

function setHeight() {
    windowHeight = $(window).innerHeight();
    $('.off-canvas, .off-canvas-wrapper').css('min-height', windowHeight);
};

function isSafari() {
    var isSafari = navigator.vendor.indexOf("Apple")==0 && /\sSafari\//.test(navigator.userAgent);
    return isSafari;
}

function cookieBanner(){

    if (document.cookie.indexOf('cookie-banner-dismiss')==-1) {
        $('.cookie-banner>.close').css('display', 'block');
    }
    else
    {
        $('.cookie-banner').css('display', 'none');
    }

    $('.cookie-banner>.close').on('click', function() {

        $('.cookie-banner').css('display', 'none');

        var setCookie = function(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        };

        setCookie('cookie-banner-dismiss', 1, 1825);
    });
}
