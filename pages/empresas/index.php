---
layout: others
class: empresas
rasca: true
title: Grupo empresarial Lidl
---

    <section class="row content">

        <div class="small-10 small-centered columns">
            <ul class="row small-up-1 medium-up-2 large-up-3">
                <li class="column">
                    Stiftsbergstraße 1<br>
                    74167 Neckarsulm<br>
                    USt-IdNr.: DE145803808
                </li>
                <li class="column">
                    Lidl E-Commerce International<br>
                    GmbH & Co. KG Stiftsbergstraße 1<br>
                    74172 Neckarsulm<br>
                    USt-IdNr.: DE814838662
                </li>
                <li class="column">
                    LIDL SUPERMERCADOS, S.A.U.<br>
                    C/ Beat Oriol, s/n. Pol. Ind. La Granja<br>
                    08110 Montcada i Reixac (Barcelona)<br>
                    C.I.F. A60195278
                </li>
            </ul>
        </div>

    </section>
