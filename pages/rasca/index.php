---
layout: others
class: rasca
rasca: true
title: Ganadores Rasca Plus
---

<?php
/*
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors',1);
*/

date_default_timezone_set('Europe/Madrid');
setlocale(LC_ALL, 'es_ES');
setlocale(LC_TIME, 'Spain');

require $_SERVER["DOCUMENT_ROOT"] . '/vendor/autoload.php';
use Carbon\Carbon;

Carbon::setLocale('es');

$dias = array("domingo","lunes","martes","miércoles","jueves","viernes","sábado");
$meses = array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");

$webservicesURL = "https://api.lidlplus.es/v2";
//$webservicesURL = "https://lidlplus-staging-api.azurewebsites.net/v2";
$uri = $webservicesURL.'/scratchcoupons/winners?winnerType=Purchase';

//Obtenemos los ganadores de Rasca Plus
$response = \Httpful\Request::get($uri)
            ->addHeader('X-Api-Key', 'd9036ddd-f024-4351-838d-1596e7f7d157')
            ->send();
$winners = $response->body;

?>

<section class="winners row">

    <div class="small-10 small-centered columns">
        <h3 class="small-12 columns">Listado de ganadores</h3>

        <div class="small-12 columns">
            <?php

                if(isset($winners) && $winners != "" && count($winners) > 0){

                    echo '
            <ul class="row small-up-1 medium-up-2">';



                        foreach($winners as $winner){

                            Carbon::setToStringFormat('d/m/Y');
                            $date = Carbon::parse($winner->Date);

                            echo '

                <li class="column">
                    <span class="date">'.$date.'</span><br>
                    <span class="name">'.$winner->Name.' '.$winner->LastName.'</span>, '.$winner->Prize->Prize.'<br>
                    Tienda: '.$winner->Store->Address.'
                </li>
                            ';
                        }

            echo '</ul>

            ';

                }else{
                    echo '<p "small-12">¡Ups! Parece que todavía no tenemos ganadores.</p>';
                }

            echo '<p class="link-bases-legales"><a href="/rasca/bases-legales.html" class="button">Ver bases legales</a></p>';
            ?>
        </div>
    </div>

</section>
