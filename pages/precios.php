---
layout: default
class: precios
precios: true
---

<?php
/*
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors',1);
*/

date_default_timezone_set('Europe/Madrid');
setlocale(LC_ALL, 'es_ES');
setlocale(LC_TIME, 'Spain');

require $_SERVER["DOCUMENT_ROOT"] . '/vendor/autoload.php';
use Carbon\Carbon;

Carbon::setLocale('es');

$dias = array("domingo","lunes","martes","miércoles","jueves","viernes","sábado");
$meses = array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");


$id = isset($_GET['id']) ? $_GET['id'] : null;

$webservicesURL = "https://api.lidlplus.es/v2";
//$webservicesURL = "https://lidlplus-staging-api.azurewebsites.net/v3";

//Obtenemos los productos con Precio Lidl Plus
$response = \Httpful\Request::get($webservicesURL.'/lidlplusprices')->send();
$productos = $response->body;

$isProducto = false;
$producto = null;
$productHeader = null;

if(isset($productos) && $productos != "" && count($productos) > 0){
    //Obtenemos fechas en base al primer productos
    $response = \Httpful\Request::get($webservicesURL.'/lidlplusprices/'.$productos[0]->Id)->send();
    $productoFecha = $response->body;
    $dateIni = Carbon::parse($productoFecha->StartValidityDate);
    $dateFin = Carbon::parse($productoFecha->EndValidityDate);


    if(isset($id)){
        $response = \Httpful\Request::get($webservicesURL.'/lidlplusprices/'.$id)->send();
        $producto = $response->body;

        if(isset($producto) && $producto != "") $isProducto = true;
    }
}

?>


    <header class="header expanded row">
        <h1>Lidl Plus</h1>

        <div class="menu-button hide-for-large" data-open="menu"><button class="menu-icon" type="button"><span class="show-for-sr">Open menu</span></button></div>
    </header>

<?php

    if(!$isProducto && isset($productos) && $productos != "" && count($productos) > 0){

        echo '

    <section class="precios-title expanded row">
        <div class="left">
            <img data-interchange="[img/ico-precios.png, small], [img/ico-precios@2x.png, retina]" alt="" width="60">
        </div>
        <div class="right">
            <h2 class="subtitle">Precios Lidl Plus</h2>
            <h3>Del '.$dateIni->day.' de '.$meses[$dateIni->month-1].' al '.$dateFin->day.' de '.$meses[$dateFin->month-1].'</h3>
        </div>
    </section>

    <section class="precios-list row">
        <div class="small-12 columns">
            <ul class="row small-up-2 medium-up-3 large-up-4">
                <div class="grid-sizer small-6 medium-4 large-3 columns"></div>';

                    foreach($productos as $producto){

                        echo '
                <li class="product-item column '.($producto->IsVeiled ? 'veiled' : '').'" data-id="'.$producto->Id.'">
                    <a href="precios.php?id='.$producto->Id.'"><span></span></a>
                    <div class="product-overlay"></div>
                    <div class="product-overlay-text">'.$producto->VeiledText.'</div>
                    <div class="product-image small-12 columns">
                        <div class="arrow"></div>
                        <div class="discount-number">'.$producto->DiscountPercentage.'</div>
                        <img src="'.$producto->ImageThumbnail.'" alt="'.$producto->Name.'">
                    </div>
                    <div class="product-text small-12 columns">
                        <div class="name small-12 columns">'.$producto->ProductTitle.'</div>
                        <div class="discount small-12 columns">
                            <div class="price small-6 columns">'.$producto->Price.' '.$producto->PriceType.'</div>
                            <div class="price-lidlplus small-6 columns">'.$producto->DiscountPrice.' <span>'.$producto->PriceType.'</span></div>
                        </div>
                    </div>
                </li>';

                    }
            echo '
            </ul>
        </div>
    </section>';

    }else if($isProducto){

        echo '

    <section class="precios-image-blured row" data-interchange="[img/lidlplusprices/'.preg_replace('/\s+/','',$producto->FamilyOfInterest).'.jpg, small], [img/lidlplusprices/'.preg_replace('/\s+/','',$producto->FamilyOfInterest).'@2x.jpg, retina]"></section>

    <section class="precios-detail '.($producto->IsVeiled ? 'veiled' : '').' row">
        <div class="precios-detail-wrapper small-10 small-offset-1 columns">
            <div class="product-image small-12 large-4 columns">
                <div class="percentage" data-interchange="[img/topo.png, small], [img/topo@2x.png, retina]">
                    '.$producto->DiscountPercentage.'
                </div>
                <div class="image small-12 columns">
                    <div class="product-overlay"></div>
                    <div class="product-overlay-text">'.$producto->VeiledText.'</div>
                    <img src="'.$producto->Image.'" alt="'.$producto->Name.'">
                </div>
                <div class="discount small-12 columns">
                    <div class="price small-6 columns">
                        <span class="price-title">Precio original</span><br>
                        <span class="tachado">'.$producto->Price.' '.$producto->PriceType.'</span>
                    </div>
                    <div class="price-lidlplus small-6 columns">'.$producto->DiscountPrice.' <span>'.$producto->PriceType.'</span></div>
                </div>
            </div>
            <div class="product-text small-12 large-8 columns">
                <div class="description small-12 columns">
                    <h2 class="name">'.$producto->ProductTitle.'</h2>
                    <p class="text">'.$producto->ProductDescription.'</p>
                </div>
                <div class="block small-12 columns">
                    <h3 class="block-title">'.$producto->Block1Title.'</h3>
                    <p class="block-text">'.$producto->Block1Description.'</p>
                </div>
                <div class="block small-12 columns">
                    <h3 class="block-title">'.$producto->Block2Title.'</h3>
                    <p class="block-text">'.$producto->Block2Description.'</p>
                </div>
            </div>
        </div>

    </section>

    <section class="precios-list row">
        <h3 class="small-10 small-offset-1 columns end">Más productos con precios Lidl Plus</h3>
        <div class="small-10 small-offset-1 columns end">
            <ul class="row small-up-2 medium-up-3 large-up-4">
                <div class="grid-sizer small-6 medium-4 large-3 columns"></div>';

                    for($i = 0; $i <= 4 && $i < count($productos); $i++){
                        $producto = $productos[$i];
                        if($producto->Id != $id){
                            echo '

                <li class="product-item column" data-id="'.$producto->Id.'">
                    <a href="precios.php?id='.$producto->Id.'"><span></span></a>
                    <div class="product-image small-12 columns">
                        <div class="arrow"></div>
                        <div class="discount-number">'.$producto->DiscountPercentage.'</div>
                        <img src="'.$producto->ImageThumbnail.'" alt="'.$producto->Name.'">
                    </div>
                    <div class="product-text small-12 columns">
                        <div class="name small-12 columns">'.$producto->ProductTitle.'</div>
                        <div class="discount small-12 columns">
                            <div class="price small-6 columns">'.$producto->Price.' '.$producto->PriceType.'</div>
                            <div class="price-lidlplus small-6 columns">'.$producto->DiscountPrice.' <span>'.$producto->PriceType.'</span></div>
                        </div>
                    </div>
                </li>';

                        }
                    }
            echo '

            </ul>
        </div>
        <p class="ver-todos small-10 small-offset-1 columns end text-right">
            <a href="/precios.php">VER TODOS</a>
        </p>
    </section>';

    }else{
        echo '

        <section class="row text-center no-disponible">
            <div class="image small-12 columns">
                <img data-interchange="[img/precios_nodisponibles.png, small], [img/precios_nodisponibles@2x.png, retina]" alt="" width="223">
            </div>
            <h2 class="title small-12 columns">Próximamente</h2>
            <p>Lo sentimos, estamos reponiendo nuestras estanterías virtuales.</p>
            <div class="content small-12 columns">
                <p><a href="/">Volver a la página principal</a></p>
            </div>

        </section>';
    }
?>
