---
layout: blank
---

<?php
require_once 'Mobile_Detect.php';
$detect = new Mobile_Detect;

if($detect->isiOS()){
    header('Location: https://itunes.apple.com/us/app/lidl-plus-zaragoza/id1133864850?ls=1&mt=8');
    exit;

}else if($detect->isAndroidOS()){

    header('Location: https://play.google.com/store/apps/details?id=com.lidl.eci.lidlplus.es');
    exit;

}else{

    header('Location: https://lidlplus.es');
    exit;
}
?>
