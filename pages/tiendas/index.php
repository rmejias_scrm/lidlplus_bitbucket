---
layout: others
class: tiendas
rasca: true
title: Tiendas Lidl Plus
---

    <section class="row content">

        <div class="small-10 small-centered columns">
            <div class="row"><h3 class="small-12 columns">Zaragoza</h3></div>
            <ul class="row small-up-1 medium-up-2">
                <li class="column">
                    <strong>Cuarte de Huerva</strong>: Carretera Valencia
                </li>
                <li class="column">
                    <strong>Utebo</strong>: Autovía de Logroño, km 12, N-232
                </li>
                <li class="column">
                    <strong>Zaragoza</strong>: Parque Oriente - C/ Balbino Orensanz, 49-51
                </li>
                <li class="column">
                    <strong>Zaragoza</strong>: Vía Hispanidad, s/n
                </li>
                <li class="column">
                    <strong>Zaragoza</strong>: Av. De Francia, nº 10
                </li>
                <li class="column">
                    <strong>Zaragoza</strong>: Av. Policía Local, s/n
                </li>
            </ul>

            <!--div class="row"><h3 class="small-12 columns">Navarra</h3></div>
            <ul class="row small-up-1 medium-up-2">
                <li class="column">
                    <strong>Burlada</strong>: P.I. Mugazuri, s/n
                </li>
                <li class="column">
                    <strong>Cordovilla</strong>: P.I. Cordovilla, s/n
                </li>
                <li class="column">
                    <strong>Pamplona</strong>: C/ Soto Aizoain, 2-92 bajo (Berriozar)
                </li>
                <li class="column">
                    <strong>Pamplona</strong>: Ctra. Sarriguren 4-5
                </li>
            </ul>

            <div class="row"><h3 class="small-12 columns">Álava</h3></div>
            <ul class="row small-up-1 medium-up-2">
                <li class="column">
                    <strong>Vitoria</strong>: C/ Domingo Beltrán, 36-38
                </li>
                <li class="column">
                    <strong>Vitoria</strong>: C/ Portal de Gamarra, 38 - B
                </li>
                <li class="column">
                    <strong>Vitoria</strong>: Alto de Armentia, s/n
                </li>
            </ul>

            <div class="row"><h3 class="small-12 columns">Vizcaya</h3></div>
            <ul class="row small-up-1 medium-up-2">
                <li class="column">
                    <strong>Amorebieta</strong>: C/ San Pedro, 56
                </li>
                <li class="column">
                    <strong>Barakaldo</strong>: C/ Molino de San Juan, 4
                </li>
                <li class="column">
                    <strong>Basauri</strong>: C/ Azganeta , 16 (Pol. Artunduaga)
                </li>
                <li class="column">
                    <strong>Bilbao</strong>: C/ Moncada, 3 (Rekalde)
                </li>
                <li class="column">
                    <strong>Bilbao</strong>: Deusto-C/  Jon Arrospide, 30
                </li>
                <li class="column">
                    <strong>Bilbao</strong>: Santutxu-C/ Iturribide, 121
                </li>
                <li class="column">
                    <strong>Bilbao</strong>: C/ Autonomía, 1
                </li>
                <li class="column">
                    <strong>Erandio</strong>: Ctra. La Avanzada s/n
                </li>
                <li class="column">
                    <strong>Erandio</strong>: Ribera de Axpe, nº 32
                </li>
                <li class="column">
                    <strong>Galdakao</strong>: Polígono Guturribal, s/n
                </li>
                <li class="column">
                    <strong>Gernika</strong>: C/ Goikoibarra, s/n
                </li>
                <li class="column">
                    <strong>Irurreta-Durango</strong>: Polígono Mellabiena, 4
                </li>
                <li class="column">
                    <strong>Santurce</strong>: Balparda - P.I. El Árbol - sector 2
                </li>
                <li class="column">
                    <strong>Sestao</strong>: Ctra. Valle Trápaga a Baracaldo (P.I. Ibarzaharra)
                </li>
            </ul-->

        </div>

    </section>
