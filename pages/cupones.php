---
layout: default
class: cupones
cupones: true
---

<?php
/*
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors',1);
*/

date_default_timezone_set('Europe/Madrid');
setlocale(LC_ALL, 'es_ES');
setlocale(LC_TIME, 'Spain');

require $_SERVER["DOCUMENT_ROOT"] . '/vendor/autoload.php';
use Carbon\Carbon;

Carbon::setLocale('es');

$dias = array("domingo","lunes","martes","miércoles","jueves","viernes","sábado");
$meses = array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");


$id = isset($_GET['id']) ? $_GET['id'] : null;

$webservicesURL = "https://api.lidlplus.es/v2";

//Autenticacion
$user = "+11111111111";
$pass = "1SRypjAKUZtQmrs";

$response = \Httpful\Request::post('https://lidlplus-prod-authentication.azurewebsites.net/token')
    ->body('grant_type=password&username=%2B11111111111&password=1SRypjAKUZtQmrs&scope=application-api')
    ->addHeaders(array(
        'Content-Type' => 'application/x-www-form-urlencoded',
        'Authorization' => 'basic ZDU0NjgxMWItMDZmNy00OWE1LThiMzItNGI2NmU3OTUyNGNhOmRkOGMyMmYyLTVlNTUtNGM5ZS05MDdhLWQyZGE4YzYxMzAzMw==',
    ))
    ->send();

$token = $response->body->access_token;

//Obtenemos los cupones de la semana
$response = \Httpful\Request::get($webservicesURL.'/coupons')
    ->addHeaders(array(
        'Authorization' => 'Bearer '. $token,
        'Accept' => 'application/json'
    ))
    ->send();

//var_dump($response->request);

$coupons = $response->body;
$isCoupon = false;
$coupon = null;

if(isset($coupons) && $coupons != ""){

    $dateIni = Carbon::parse($coupons[0]->StartValidityDate);
    $dateFin = Carbon::parse($coupons[0]->EndValidityDate);


    if(isset($id)){
        foreach($coupons as $couponItem){
            if($couponItem->Id == $id){
                $isCoupon = true;
                $coupon = $couponItem;
            }
        }
    }
}

?>


    <header class="header expanded row">
        <h1>Lidl Plus</h1>

        <div class="menu-button hide-for-large" data-open="menu"><button class="menu-icon" type="button"><span class="show-for-sr">Open menu</span></button></div>
    </header>

<?php

    if(!$isCoupon && isset($coupons) && $coupons != ""){

        echo '
    <section class="cupones-title expanded row">
            <div class="left">
                <img data-interchange="[img/ico-cupones.png, small], [img/ico-cupones@2x.png, retina]" alt="" width="61">
            </div>
            <div class="right">
                <h2 class="subtitle">Cupones de la semana</h2>
                <h3>Del '.$dateIni->day.' de '.$meses[$dateIni->month-1].' al '.$dateFin->day.' de '.$meses[$dateFin->month-1].'</h3>
            </div>
    </section>

    <section class="cupones-list row">
        <div class="small-10 small-offset-1 columns">
            <ul class="row small-up-1 medium-up-2 large-up-3">';

                    foreach($coupons as $coupon){
                        $withDescription = ($coupon->OfferDescription != '' ? 'with-offerdescription' : '');
                        if(!$coupon->Testcoupon){

                        echo '
                <li class="column" data-id="'.$coupon->Name.'" data-promo="'.$coupon->PromoId.'" data-ean="'.$coupon->EANCode.'" data-type="'.$coupon->Type.'" data-family="'.$coupon->FamilyOfInterest.'">
                    <a href="cupones.php?id='.$coupon->Id.'"><span></span></a>
                    <div class="crop small-12 columns"><img src="'.$coupon->Image.'" alt="'.$coupon->Description.'"></div>
                    <div class="coupon-text small-12 columns '.$withDescription.'">
                        <div class="name small-8 columns">'.$coupon->Product.'</div>
                        <div class="discount small-4 columns">
                            <span class="discount-number">'.$coupon->Discount.'</span>
                            <span class="discount-type">'.$coupon->OfferDescription.'</span>
                        </div>
                    </div>
                </li>';
                        }
                    }
        echo '
            </ul>
        </div>
    </section>';


    }else if($isCoupon){
        $withDescription = ($coupon->OfferDescription != '' ? 'with-offerdescription' : '');

        echo '
    <section class="coupon-image-blured row" style="background-image:url('.$coupon->Image.');"></section>

    <section class="coupon-detail row">
        <div class="coupon-detail-wrapper small-10 small-offset-1 large-8 large-offset-2 columns">
            <div class="crop small-12 columns"><img src="'.$coupon->Image.'" alt="'.$coupon->Description.'"></div>
            <div class="coupon-text small-12 columns '.$withDescription.'">
                <div class="description small-8 medium-9 columns">
                    <span class="name">'.$coupon->Product.'</span>
                    <span class="text">'.$coupon->Description.'</span>
                </div>
                <div class="discount small-4 medium-3 columns">
                    <span class="discount-number">'.$coupon->Discount.'</span>
                    <span class="discount-type">'.$coupon->OfferDescription.'</span>
                </div>
            </div>
            <div class="coupon-legal small-12 columns">
                <p>
                    <strong>'.$coupon->FooterTitle.'</strong><br>
                    '.$coupon->FooterDescription.'
                </p>
            </div>
        </div>

    </section>

    <section class="cupones-list from-coupon-detail row">
        <div class="cupones-list-wrapper small-10 small-offset-1 large-8 large-offset-2 columns">
            <h3>Más cupones</h3>
            <ul class="row small-up-1 medium-up-2">';

                    foreach($coupons as $coupon){
                        if($coupon->Id != $id){
                            $withDescription = ($coupon->OfferDescription != '' ? 'with-offerdescription' : '');
                            if(!$coupon->Testcoupon){

                            echo '
                <li class="column" data-id="'.$coupon->Name.'" data-promo="'.$coupon->PromoId.'" data-ean="'.$coupon->EANCode.'" data-type="'.$coupon->Type.'" data-family="'.$coupon->FamilyOfInterest.'">
                    <a href="cupones.php?id='.$coupon->Id.'"><span></span></a>
                    <div class="crop small-12 columns"><img src="'.$coupon->Image.'" alt="'.$coupon->Description.'>"></div>
                    <div class="coupon-text small-12 columns '.$withDescription.'">
                        <div class="name small-8 columns">'.$coupon->Product.'</div>
                        <div class="discount small-4 columns">
                            <span class="discount-number">'.$coupon->Discount.'</span>
                            <span class="discount-type">'.$coupon->OfferDescription.'</span>
                        </div>
                    </div>
                </li>';
                            }

                        }
                    }
        echo '
            </ul>
        </div>
    </section>';

    }else{

        echo '

        <section class="row text-center no-disponible">
            <div class="image small-12 columns">
                <img data-interchange="[img/cupones_nodisponibles.png, small], [img/cupones_nodisponibles@2x.png, retina]" alt="" width="265">
            </div>
            <h2 class="title small-12 columns">Próximamente</h2>
            <p>Lo sentimos, estamos reponiendo nuestras estanterías virtuales.</p>
            <div class="content small-12 columns">
                <p><a href="/">Volver a la página principal</a></p>
            </div>

        </section>';


    }
?>
